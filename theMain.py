import numpy as np

grid=np.zeros((3,3,3,3), dtype=np.str)
matrix=np.array([["0","1","2"],
        ["3","4","5"],
        ["6","7","8"]])

ThewinX=np.array([["X","X","X"],
        ["X","X","X"],
        ["X","X","X"]])
ThewinO=np.array([["O","O","O"],
        ["O","O","O"],
        ["O","O","O"]])
player="O"
player2="X"

def slant_check(matrix,Player):
    if matrix[0][0] == Player and matrix[1][1] == Player and matrix[2][2] == Player:
        return True
    if matrix[2][0] == Player and matrix[1][1] == Player and matrix[0][2] == Player:
        return True
    return False

def vertical_check(matrix,Player):
    for i in range(len(matrix) - 2):
        for j in range(len(matrix)):
            if matrix[i][j] == Player and matrix[i + 1][j] == Player and matrix[i + 2][j] == Player:
                return True
    return False

def horizontal_check(matrix,Player):
    for i in range(len(matrix)):
        for j in range(len(matrix) - 2):
            if matrix[i][j]== Player and matrix[i][j + 1]== Player and matrix[i][j + 2]== Player :
                return True

def IsTheBostredThere(o,t):
    r=False
    for i in range(3):
        for j in range(3):
            if o==t[i][j]:
                r=True
    return r

def print3D(grid):
    print("",grid[0,0,0],grid[1,0,0],grid[2,0,0],"\n",grid[0,0,1],grid[1,0,1],grid[2,0,1],"\n",grid[0,0,2],grid[1,0,2],grid[2,0,2],"\n""\n",grid[0,1,0],grid[1,1,0],grid[2,1,0],"\n",grid[0,1,1],grid[1,1,1],grid[2,1,1],"\n",grid[0,1,2],grid[1,1,2],grid[2,1,2],"\n","\n",grid[0,2,0],grid[1,2,0],grid[2,2,0],"\n",grid[0,2,1],grid[1,2,1],grid[2,2,1],"\n",grid[0,2,2],grid[1,2,2],grid[2,2,2],"\n")
    print("-----------------------------------------------------------------------------------------")

def cheseYuerGrid(Player,matrix):
    print(matrix)
    Playerbord=int(input("on what bord to put player {}\n" .format(Player)))
    if False==IsTheBostredThere(str(Playerbord),matrix):
        print("not valid")
        Playerbord=int(input("on what bord to put player {}\n" .format(Player)))
    if Playerbord<=2:
        tmp=0
    elif Playerbord<=5:
        tmp=1
    elif Playerbord<=8:
        tmp=2
    Playerbord=Playerbord%3
    return Playerbord,tmp

def convertTheGrid(grid,player,player2,way="Ford"):
    matrix=np.array([["0","1","2"],
            ["3","4","5"],
            ["6","7","8"]])
    matrix2=np.array([["","",""],
            ["","",""],
            ["","",""]])
    if way=="Ford":
        #print(matrix)
        originalNumber=-1
        for j in grid:
            for i in j:
                originalNumber=originalNumber+1
                if i==player:
                    number=originalNumber
                    if number<=2:
                        tmp=0
                    elif number<=5:
                        tmp=1
                    elif number<=8:
                        tmp=2
                    number=number%3
                    #print(number)
                    matrix[tmp,number]=player

                if i==player2:
                    number=originalNumber
                    if number<=2:
                        tmp=0
                    elif number<=5:
                        tmp=1
                    elif number<=8:
                        tmp=2
                    number=number%3
                    #print(number)
                    matrix[tmp,number]=player2
        return matrix
    elif way=="back":
        #print(matrix)
        originalNumber=-1
        for j in grid:
            for i in j:
                originalNumber=originalNumber+1
                if i==player:
                    number=originalNumber
                    if number<=2:
                        tmp=0
                    elif number<=5:
                        tmp=1
                    elif number<=8:
                        tmp=2
                    number=number%3
                    #print(number)
                    matrix2[tmp,number]=player
                elif i==player2:
                    number2=originalNumber
                    if number2<=2:
                        tmp2=0
                    elif number2<=5:
                        tmp2=1
                    elif number2<=8:
                        tmp2=2
                    number2=number2%3
                    #print(number)
                    matrix2[tmp2,number2]=player2
        return matrix2

def theOne(player,player2,Thewin,grid,matrix):
    theEnd=False
    tmp,move1=cheseYuerGrid(player,matrix)
    grid2=grid[tmp,move1]
    grid[tmp,move1]=convertTheGrid(grid2,player,player2)
    print3D(grid)
    message=("its {} turn\n").format(player)
    move2=int(input(message))
    if False==IsTheBostredThere(str(move2),grid2):
        print("not valid")
        message=("its {} turn\n").format(player)
        move2=int(input(message))
    if move2<=2:
        tmp2=0
    elif move2<=5:
        tmp2=1
    elif move2<=8:
        tmp2=2
    move2=move2%3
    grid[tmp,move1,tmp2,move2]=player
    grid[tmp,move1]=convertTheGrid(grid2,player,player2,"back")
    win1=horizontal_check(grid[tmp,move1],player)
    win2=slant_check(grid[tmp,move1],player)
    win3=vertical_check(grid[tmp,move1],player)
    if win1 or win2 or win3 :
        grid[tmp,move1]=Thewin
        matrix[move1,tmp]=player
        win1=False
        win2=False
        win3=False
        win1=horizontal_check(matrix,player)
        win2=slant_check(matrix,player)
        win3=vertical_check(matrix,player)
        if win1 or win2 or win3 :
            print(matrix)
            print("Player {} winssssssssssssssssssssssssssssssssssssss".format(player))
            theEnd=True
        win1=False
        win2=False
        win3=False
    print3D(grid)
    if matrix[tmp2,move2]==player or matrix[tmp2,move2]==player2:
        TheNext=True
    else:
        TheNext=False
    return grid,matrix,move2,tmp2,theEnd,TheNext


def TheTow(player,player2,Thewin,grid,matrix,move,tmp):
    theEnd=False
    next=False
    grid[move,tmp]=convertTheGrid(grid[move,tmp],player,player2)
    message=("its {} turn\n").format(player)
    print3D(grid)
    move2=int(input(message))
    if False==IsTheBostredThere(str(move2),grid[move,tmp]):
        print("not valid")
        message=("its {} turn\n").format(player)
        move2=int(input(message))
    if move2<=2:
        tmp2=0
    elif move2<=5:
        tmp2=1
    elif move2<=8:
        tmp2=2
    move2=move2%3
    grid[move,tmp,tmp2,move2]=player
    win1=horizontal_check(grid[move,tmp],player)
    win2=slant_check(grid[move,tmp],player)
    win3=vertical_check(grid[move,tmp],player)
    if win1 or win2 or win3 :
        grid[move,tmp]=Thewin
        matrix[tmp,move]=player
        win1=False
        win2=False
        win3=False
        win1=horizontal_check(matrix,player)
        win2=slant_check(matrix,player)
        win3=vertical_check(matrix,player)
        if win1 or win2 or win3 :
            print(matrix)
            print("Player {} winssssssssssssssssssssssssssssssssssssss".format(player2))
            theEnd=True
        win1=False
        win2=False
        win3=False
    grid[move,tmp]=convertTheGrid(grid[move,tmp],player,player2,"back")
    print3D(grid)
    if matrix[tmp2,move2]==player or matrix[tmp2,move2]==player2:
        TheNext=True
    else:
        TheNext=False
    return grid,matrix,move2,tmp2,theEnd,TheNext


def theInput(grid,matrix,player,player2,ThewinO,ThewinX):
    next=False
    turn=-1
    while True:
        turn=turn+1
        if turn==0 or TheNext:
            #1
            grid,matrix,move,tmp,theEnd,TheNext=theOne(player,player2,ThewinO,grid,matrix)
            if theEnd:
                break
            #2
            if TheNext:
                grid,matrix,move2,tmp2,theEnd,TheNext=theOne(player2,player,ThewinX,grid,matrix)
            else:
                grid,matrix,move2,tmp2,theEnd,TheNext=TheTow(player2,player,ThewinX,grid,matrix,move,tmp)
            if theEnd:
                break
            move,tmp=move2,tmp2
        elif turn>=1:
            #3
            if TheNext:
                grid,matrix,move2,tmp2,theEnd,TheNext=theOne(player,player2,ThewinO,grid,matrix)
            else:
                grid,matrix,move2,tmp2,theEnd,TheNext=TheTow(player,player2,ThewinO,grid,matrix,move,tmp)
            if theEnd:
                break
            #3
            if TheNext:
                grid,matrix,move,tmp,theEnd,TheNext=theOne(player2,player,ThewinX,grid,matrix)
            else:
                grid,matrix,move,tmp,theEnd,TheNext=TheTow(player2,player2,ThewinX,grid,matrix,move2,tmp2)
            if theEnd:
                break


theInput(grid,matrix,player,player2,ThewinO,ThewinX)
